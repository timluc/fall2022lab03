//Khang Tim Luc
//2032290
public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] b1 = new Bicycle[4];
        b1[0] = new Bicycle("Bike 1", 4, 100);
        b1[1] = new Bicycle("Bike 2", 34, 1000);
        b1[2] = new Bicycle("Bike 3", 101, 10000);
        b1[3] = new Bicycle("Bike 4", 67, 100000);
        for (int i = 0; i < b1.length; i++) {
            System.out.println(b1[i]);
        }
    }
}
